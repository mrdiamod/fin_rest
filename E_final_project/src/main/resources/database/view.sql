create view viewproducts as
select `pr`.`name` AS `nameProd`, c.name AS `nameCateg`, c.id AS `idCateg`
from (`dbfinpro`.`product` `pr`
    left join `dbfinpro`.`prod_categ` `prca` on (`pr`.`id` = `prca`.`category_id`))
         left join `dbfinpro`.category c on (prca.category_id = c.id);


create definer = root@localhost view categoryone as
select `vp`.`nameProd` AS `nameProd`, `vp`.`nameCateg` AS `nameCateg`
from `dbfinpro`.`viewproducts` `vp`
where (`vp`.`idCateg` = 1);

