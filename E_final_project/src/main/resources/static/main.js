var app = angular.module("ProductManagement", ["angularUtils.directives.dirPagination"]);

// Controller Part
app.controller("ProductController", function ($scope, $http) {
    let minValue = 0;
    let maxValue = 0;

    $scope.range = {
        minValue: "",
        maxValue: ""
    };

    $scope.products = {
        totalPages : ""
    };
    $scope.pages = [];
    $scope.pageN = "";

   /* $scope.productForm = {
        id: -1,
        name: "",
        categories: [{
            id: "",
            name: "",
            subcategoryes: [{
                id: "",
                name: "",
                subcategoryes: []
            }]
        }],
        price: {
            productId: -1,
            value: 0,
            currency: ""
        }
    };*/
    $scope.productForm = {
         id: -1,
            name: "",
            categories: [{
                id: "",
                name: "",
                subcategoryes: [{
                    id: "",
                    name: "",
                    subcategoryes: []
                }]
            }],
            price: {
                productId: -1,
                value: 0,
                currency: ""
            }
    };

    // Now load the data from server
    _refreshProductData();
    _paginationGetAllPage();
    // HTTP POST/PUT methods for add/edit product
    //("/api/v1/product/")
    // Call: http://localhost:8090/api/v1/product
    $scope.submitProduct = function () {
        var method = "";
        var url = "";

        if ($scope.productForm.id == -1) {
            method = "POST";
            url = '/api/v1/product/';
        } else {
            method = "PUT";
            url = '/api/v1/product/';
        }

        $http({
            method: method,
            url: url,
            data: angular.toJson($scope.productForm),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(_success, _error);
    };

    // HTTP GET- get products by range min max value
    // Call: http://localhost:8090/api/v1/product/{minValue},{maxValue}
    $scope.productByRange = function (range) {
        $scope.range.minValue = range.minValue;
        $scope.range.maxValue = range.maxValue;
        $http({
            method: 'GET',
            url: '/api/v1/product/' + range.minValue +'/' + range.maxValue
        }).then(
            function (res) { // success
                $scope.products = res.data;
            },
            _error);
        /*if ($scope.myForm.$valid) {
            $('#reset-btn').show()
            $('#show').show()
        }*/
    };

    $scope.pagingPage = function (page) {
        /*$scope.range.minValue = range.minValue;
        $scope.range.maxValue = range.maxValue;*/
        $http({
            method: 'GET',
            url: '/api/v1/product/page/' + page
        }).then(
            function (res) { // success
                $scope.products = res.data;
            },
            _error);
        /*if ($scope.myForm.$valid) {
            $('#reset-btn').show()
            $('#show').show()
        }*/
    };

    $scope.clearFormDataRange = function() {
        _refreshProductData();
        _clearFormData();
    };

    $scope.createProduct = function () {
        _clearFormData();
    }

    // HTTP DELETE- delete product by Id
    // Call: http://localhost:8090/api/v1/product/{id}
    $scope.deleteProduct = function (product) {
        $http({
            method: 'DELETE',
            url: '/api/v1/product/' + product.id
        }).then(_success, _error);
    };

    // In case of edit
    $scope.editProduct = function (product) {
        $scope.productForm.id = product.id;
        $scope.productForm.name = product.name;
        $scope.productForm.categories = product.categories;
        $scope.productForm.price = product.price;
    };

    // Private Method
    // HTTP GET- get all products collection
    // Call: http://localhost:8090/api/v1/product
    function _refreshProductData() {
        $http({
            method: 'GET',
            url: '/api/v1/product/'
        }).then(
            function (res) { // success
                $scope.products = res.data;
                let pagesS = $scope.products.totalPages;
                _paginationGetAllPage(pagesS);
            },
            function (res) { // error
                console.log("Error: " + res.status + " : " + res.data);
            }
        );
    }

    function _paginationGetAllPage(pagesS){
        for (var i = 0; i < pagesS; i++) {
            $scope.pages.push(i);
        }
    }

    function _success(res) {
        _refreshProductData();
        _clearFormData();
    }

    function _error(res) {
        var data = res.data;
        var status = res.status;
        var header = res.header;
        var config = res.config;
        alert("Error: " + status + ":" + data);
    }

    // Clear the form
    function _clearFormData() {
        $scope.productForm.id = -1;
        $scope.productForm.name = "";
        /*11 $scope.productForm.categories= "";
         $scope.productForm.price.value = ""*/
        /* 22 $scope.productForm.categories.name = "";
          $scope.productForm.price.value = "";
          $scope.productForm.price.currency = ""*/
        $scope.productForm.categories = [{}];
        $scope.productForm.price = {};

        $scope.pages = {};
    }

});