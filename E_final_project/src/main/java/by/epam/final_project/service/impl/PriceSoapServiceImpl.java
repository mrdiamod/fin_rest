package by.epam.final_project.service.impl;

import by.epam.final_project.exception.PriceException;
import by.epam.final_project.service.PriceSoapService;
import by.epam.final_project.soap.PriceClient;
import by.epam.final_project.soap.PriceConfiguration;
import by.epam.final_project.soap.gen.*;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class PriceSoapServiceImpl implements PriceSoapService {
    private static final Logger LOGGER = Logger.getLogger(PriceSoapServiceImpl.class);
    private PriceClient priceClient;

    @PostConstruct
    public void initData() {
        PriceConfiguration priceConfiguration = new PriceConfiguration();
        Jaxb2Marshaller marshaller = priceConfiguration.marshaller();
        priceClient = priceConfiguration.priceClient(marshaller);
    }

    /**
     * Method get price by id
     * @param id Product, type Long
     * @return Price by Product id, type class {@link Price}
     */
    @Override
    public Price get(Long id) {
        try {
            GetPriceResponse getResp = priceClient.getPrice(id);
            return getResp.getPrice();
        } catch (Exception e) {
            LOGGER.info(e.toString());
            throw new PriceException(e.getMessage(), e, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Method get price by range min and max value
     *
     * @param minValue min value price, type Long
     * @param maxValue max value price, type Long
     * @return List Prices, type class {@link List}
     * and {@link Price}
     */
    @Override
    public List<Price> getByRange(Long minValue, Long maxValue) {
        try {
            GetPricesRangeResponse getPricesRangeResponse = priceClient.getPricesRange(minValue, maxValue);
            return getPricesRangeResponse.getPricelist();
        } catch (Exception e) {
            LOGGER.info(e.toString());
            throw new PriceException(e.getMessage(), e, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Method save price in Soap Service
     * @param price class {@link Price}
     * @return String with status message
     * create in SOAP service
     */
    @Override
    public String save(Price price) {
        try {
            Long idProd = price.getProductId();
            int value = price.getValue();
            CurrencyEnum currency = price.getCurrency();
            CreatePriceResponse getResp = priceClient.createPrice(idProd, value, currency);
            return getResp.getStatus();
        } catch (Exception e) {
            LOGGER.info(e.toString());
            throw new PriceException(e.getMessage(), e, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Method update price by id product in SoapService
     * @param price class {@link Price}
     * @return String with status message
     * update in SOAP service
     */
    @Override
    public String update(Price price) {
        try {
            Long idProd = price.getProductId();
            int value = price.getValue();
            CurrencyEnum currency = price.getCurrency();
            UpdatePriceResponse getResp = priceClient.updatePrice(idProd, value, currency);
            return getResp.getStatus();
        } catch (Exception e) {
            LOGGER.info(e.toString());
            throw new PriceException(e.getMessage(), e, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Method delete price by id product in SoapService
     * @param id idProduct, type Long
     * @return String with status message
     * delete in SOAP service
     */
    @Override
    public String delete(Long id) {
        try {
            DeletePriceResponse getResp = priceClient.deletePrice(id);
            return getResp.getStatus();
        } catch (Exception e) {
            LOGGER.info(e.toString());
            throw new PriceException(e.getMessage(), e, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }
}
