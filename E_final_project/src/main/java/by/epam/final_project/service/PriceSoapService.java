package by.epam.final_project.service;

import by.epam.final_project.soap.gen.Price;

import java.util.List;

/**
 *  Interface for Soap Service Price
 */
public interface PriceSoapService {
    /**
     * Method get price by id
     * @param id Product, type Long
     * @return Price by Product id, type class {@link Price}
     */
    Price get(Long id);

    /**
     * Method get price by range min and max value
     * @param minValue min value price, type Long
     * @param maxValue max value price, type Long
     * @return List Prices, type class {@link List}
     * and {@link Price}
     */
    List<Price> getByRange(Long minValue, Long maxValue);

    /**
     * Method save price in Soap Service
     * @param price class {@link Price}
     * @return String with status message
     * create in SOAP service
     */
    String save(Price price);

    /**
     * Method update price by id product in SoapService
     * @param price class {@link Price}
     * @return String with status message
     * update in SOAP service
     */
    String update(Price price);

    /**
     * Method delete price by id product in SoapService
     * @param id idProduct, type Long
     * @return String with status message
     * delete in SOAP service
     */
    String delete(Long id);

}
