package by.epam.final_project.service.impl;

import by.epam.final_project.entity.Product;
import by.epam.final_project.exception.UsersException;
import by.epam.final_project.repository.ProductRepository;
import by.epam.final_project.service.ProductService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {
    private static final Logger LOGGER = Logger.getLogger(ProductServiceImpl.class);
    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    /**
     * Method get product by id
     * @param id Product, type Long
     * @return Poduct by id, type class {@link Product}
     */
    @Override
    public Product getById(Long id) {
        LOGGER.info("IN ProductServiceImpl getById by ID: " + id);
        Optional<Product> optionalProduct = productRepository.findById(id);
        if (optionalProduct.isPresent()) {
            return optionalProduct.get();
        } else {
            String messageError = "Not found product with ID: " + id;
            LOGGER.error(messageError);
            throw new UsersException(messageError);
        }
    }

    /**
     * Method save product by id in DB
     * @param product class {@link Product}
     */
    @Override
    public void save(Product product) {
        LOGGER.info("IN ProductServiceImpl save by product: " + product);
        productRepository.save(product);
    }

    /**
     * Method update product by id in DB
     * @param product class {@link Product}
     */
    @Override
    public void update(Product product) {
        LOGGER.info("IN ProductServiceImpl update by product: "
                + product.getId() + ";" + product.getName());
        productRepository.save(product);
    }

    /**
     * Method delete product by id in DB
     * @param id product, type Long
     */
    @Override
    public void delete(Long id) {
        LOGGER.info("IN ProductServiceImpl delete by ID: " + id);
        productRepository.deleteById(id);
    }

    /**
     * Method get list all products
     * @return List product's
     */
    @Override
    public Page<Product> getAll(Pageable pageable) {
        LOGGER.info("IN ProductServiceImpl getAll");
        return productRepository.findAll(pageable);
    }

    /**
     * find last id Product in DB
     * @return Long id
     */
    @Override
    public Long getProductLastId() {
        LOGGER.info("IN ProductServiceImpl getProductLastId");
        Product lastId = productRepository.findFirstIdByOrderByIdDesc();
        return lastId.getId();
    }
}
