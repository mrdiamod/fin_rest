package by.epam.final_project.service.impl;

import by.epam.final_project.entity.Category;
import by.epam.final_project.exception.UsersException;
import by.epam.final_project.repository.CategoryRepository;
import by.epam.final_project.service.CategoryService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {
    private static final Logger LOGGER = Logger.getLogger(CategoryServiceImpl.class);
    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Category getById(Long id) {
        LOGGER.info("IN CategoryServiceImpl getById by ID: " + id);
        Optional<Category> optionalCategory = categoryRepository.findById(id);
        if (optionalCategory.isPresent()) {
            return optionalCategory.get();
        } else {
            String messageError = "Not found category with ID: " + id;
            LOGGER.error(messageError);
            throw new UsersException(messageError);
        }
    }

    /**
     * find last save id Category in DB
     * @return Category by last save, type class {@link Category}
     */
    @Override
    public Category getCategoryByLastSaveId() {
        LOGGER.info("IN CategoryServiceImpl getCategoryByLastSaveId");
        Category lastSaveCategory = categoryRepository.findFirstIdByOrderByIdDesc();
        LOGGER.info("Getting id category " + lastSaveCategory.getId());
        return lastSaveCategory;
    }

    /**
     * Method get category by name
     * @param name Category, type String
     * @return Category by name, type class {@link Category}
     */
    @Override
    public Category getByName(String name) {
        LOGGER.info("IN CategoryServiceImpl getByName by category name: " + name);
        return categoryRepository.findByName(name);
    }

    @Override
    public void save(Category category) {
        LOGGER.info("IN CategoryServiceImpl save by category: " + category);
        categoryRepository.save(category);
    }

    /**
     * Method update category by id in DB
     *
     * @param category class {@link Category}
     */
    @Override
    public void update(Category category) {
        LOGGER.info("IN CategoryServiceImpl update by category: " + category);
        categoryRepository.save(category);
    }

    @Override
    public void delete(Long id) {
        LOGGER.info("IN CategoryServiceImpl delete by ID: " + id);
        categoryRepository.deleteById(id);
    }

    @Override
    public List<Category> getAll() {
        LOGGER.info("IN CategoryServiceImpl getAll");
        return categoryRepository.findAll();
    }
}
