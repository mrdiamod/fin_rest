package by.epam.final_project.service;

import by.epam.final_project.entity.User;

import java.util.List;

/**
 *  Interface Service for class User
 */

public interface UserService {
    /**
     *  Method get list all users
     * @return List user's
     */
    List<User> getAll();
}
