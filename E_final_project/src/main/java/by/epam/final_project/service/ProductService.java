package by.epam.final_project.service;

import by.epam.final_project.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *  Interface Service for class Product
 */

public interface ProductService {
    /**
     * Method get product by id
     * @param id Product, type Long
     * @return Poduct by id, type class {@link Product}
     */
    Product getById(Long id);

    /**
     *  Method save product by id in DB
     * @param product class {@link Product}
     */
    void save(Product product);

    /**
     *  Method update product by id in DB
     * @param product class {@link Product}
     */
    void update(Product product);

    /**
     * Method delete product by id in DB
     * @param id product, type Long
     */
    void delete(Long id);

    /**
     *  Method get list all products
     * @return List product's
     */
    Page<Product> getAll(Pageable pageable);

    /**
     * find last id Product in DB
     * @return Long id
     */
    Long getProductLastId();
}
