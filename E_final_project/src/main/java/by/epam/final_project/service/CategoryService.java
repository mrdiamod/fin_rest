package by.epam.final_project.service;

import by.epam.final_project.entity.Category;

import java.util.List;

/**
 *  Interface Service for class Category
 */
public interface CategoryService {
    /**
     * Method get category by id
     * @param id Category, type Long
     * @return Category by id, type class {@link Category}
     */
    Category getById(Long id);

    /**
     * find last save id Category in DB
     * @return Category by last save, type class {@link Category}
     */
    Category getCategoryByLastSaveId();

    /**
     * Method get category by name
     * @param name Category, type String
     * @return Category by name, type class {@link Category}
     */
    Category getByName(String name);

    /**
     * Method save category by id in DB
     * @param category class {@link Category}
     */
    void save(Category category);

    /**
     * Method update category by id in DB
     * @param category class {@link Category}
     */
    void update(Category category);

    /**
     * Method delete category by id in DB
     * @param id category, type Long
     */
    void delete(Long id);

    /**
     *  Method get list all categories
     * @return List category's
     */
    List<Category> getAll();


}
