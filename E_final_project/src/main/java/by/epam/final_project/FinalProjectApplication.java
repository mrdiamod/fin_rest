package by.epam.final_project;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalProjectApplication {
    private static final Logger LOGGER = Logger.getLogger(FinalProjectApplication.class);

    public static void main(String[] args) {
        LOGGER.info("run rest app...wait..");
        SpringApplication.run(FinalProjectApplication.class, args);
        LOGGER.info(" rest app started!");

    }
}
