package by.epam.final_project.controller.rest;

import by.epam.final_project.entity.Category;
import by.epam.final_project.entity.Product;
import by.epam.final_project.service.CategoryService;
import by.epam.final_project.service.PriceSoapService;
import by.epam.final_project.service.ProductService;
import by.epam.final_project.soap.gen.Price;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

import static by.epam.final_project.Const.PAGINATION_SIZE;

/**
 * Rest Controller Product
 */
@RestController
@RequestMapping("/api/v1/product/")
public class ProductRestControllerV1 {
    private static final Logger LOGGER = Logger.getLogger(ProductRestControllerV1.class);
    private ProductService productService;
    private CategoryService categoryService;
    private PriceSoapService priceSoapService;

    @Autowired
    public ProductRestControllerV1(ProductService productService,
                                   CategoryService categoryService,
                                   PriceSoapService priceSoapService) {
        this.productService = productService;
        this.categoryService = categoryService;
        this.priceSoapService = priceSoapService;
    }

    @RequestMapping(value = "{id}",
            method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    public ResponseEntity<Product> getProduct(@PathVariable("id") Long productId) {
        LOGGER.info("IN ProductRestControllerV1 getProduct is product " + productId);
        if (productId == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Product product = productService.getById(productId);
        if (product == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            Long idProd = product.getId();
            product.setPrice(priceSoapService.get(idProd));
        }
        LOGGER.info("IN ProductRestControllerV1 getProduct is product " + product);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @RequestMapping(value = "",
            method = RequestMethod.POST,
            produces = {MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    public ResponseEntity<Product> saveProduct(@RequestBody @Valid Product product) {
        LOGGER.info("IN ProductRestControllerV1 saveProduct is product " + product);
        HttpHeaders headers = new HttpHeaders();
        if (product == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        productService.save(product);
        Long lastIdProduct = productService.getProductLastId();
        product.getPrice().setProductId(lastIdProduct);
        String soapResp = priceSoapService.save(product.getPrice());
        if (soapResp.equals("ERROR CREATE")) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(product, headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "",
            method = RequestMethod.PUT,
            produces = {MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    public ResponseEntity<Product> updateProduct(@RequestBody @Valid Product product) {//
        LOGGER.info("IN ProductRestControllerV1 updateProduct is product " + product);
        HttpHeaders headers = new HttpHeaders();
        if (product == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        updateCategoryProduct(product);
        Price findPrice = priceSoapService.get(product.getId());
        if (findPrice == null) {
            if (product.getPrice() == null) {
                findPrice = new Price();
                product.setPrice(findPrice);
            }
            product.getPrice().setProductId(product.getId());
            priceSoapService.save(product.getPrice());
        } else {
            String soapResp = priceSoapService.update(product.getPrice());
            if (soapResp.equals("ERROR UPDATE")) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }
        return new ResponseEntity<>(product, headers, HttpStatus.OK);
    }

    private void updateCategoryProduct(Product product) {
        for (Category category : product.getCategories()) {
            Category categoryFindByName = categoryService.getByName(category.getName());
            if (categoryFindByName == null) {
                category.setId(null);
                categoryService.save(category);
            } else {
                category.setId(categoryFindByName.getId());
                category.setSubcategoryes(categoryFindByName.getSubcategoryes());
            }
        }
        productService.update(product);
    }


    @RequestMapping(value = "{id}", method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Product> deleteProduct(@PathVariable("id") Long id) {
        Product product = productService.getById(id);
        if (product == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        productService.delete(id);
        String soapResp = priceSoapService.delete(product.getId());
        if (soapResp.equals("ERROR DELETE")) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        LOGGER.info("IN ProductRestControllerV1");
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "{minPrice}/{maxPrice}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<Product>> getProductsByRangePrice(
            @PathVariable("minPrice") Long minPrice,
            @PathVariable("maxPrice") Long maxPrice) {
        LOGGER.info("IN ProductRestControllerV1 getProductsByRangePrice");
        Pageable productPages = PageRequest.of(0, PAGINATION_SIZE);
        if (minPrice == null || maxPrice == null ||
                minPrice < 0 || maxPrice < 0 || maxPrice < minPrice) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<Price> getPricesRange = priceSoapService.getByRange(minPrice, maxPrice);
        List<Product> productsList = new ArrayList<>();
        for (Price price : getPricesRange) {
            Product product = productService.getById(price.getProductId());
            if (product != null) {
                product.setPrice(price);
                productsList.add(product);
            }
        }
        Page<Product> prodPage = new PageImpl<>(productsList, productPages, productsList.size());
        return new ResponseEntity<>(prodPage, HttpStatus.OK);
    }


    @RequestMapping(value = "", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Page<Product>> getAllProduct(@RequestParam(defaultValue = "0") int page) {
        LOGGER.info("IN ProductRestControllerV1 getAllProduct");
        return getProdusts(page);
    }

    @RequestMapping(value = "page/{page}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Page<Product>> getAllProductsPage(@PathVariable("page") int page) {
        LOGGER.info("IN ProductRestControllerV1 getAllProductsPage");
        return getProdusts(page);
    }

    private ResponseEntity<Page<Product>> getProdusts(int numberPage) {
        Pageable productPages = PageRequest.of(numberPage, PAGINATION_SIZE);
        Page<Product> products = productService.getAll(productPages);
        if (products.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            for (Product product : products) {
                Long idProd = product.getId();
                product.setPrice(priceSoapService.get(idProd));
            }
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }
}
