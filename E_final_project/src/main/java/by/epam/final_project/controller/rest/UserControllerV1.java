package by.epam.final_project.controller.rest;

import by.epam.final_project.entity.User;
import by.epam.final_project.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserControllerV1 {
    private static final Logger LOGGER = Logger.getLogger(UserControllerV1.class);
    private UserService userService;

    @Autowired
    public UserControllerV1(UserService userService) {
        this.userService = userService;

    }

    @RequestMapping(value = "", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<User>> getAllUsers(@RequestParam(defaultValue = "0") int page) {
        LOGGER.info("IN UserControllerV1 getAllUsers");
        //return userService.getAll();
         List<User> users = this.userService.getAll();
        if (users.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(users, HttpStatus.OK);
    }
}
