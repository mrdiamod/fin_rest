package by.epam.final_project;

public class Const {
    public static final String URL_SOAP_SERVICE_PRICE = "http://localhost:8070/ws/prices";
    public static final int PAGINATION_SIZE = 5;
}
