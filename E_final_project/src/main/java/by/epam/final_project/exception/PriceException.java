package by.epam.final_project.exception;

import org.springframework.http.HttpStatus;

public class PriceException extends RuntimeException {
    private HttpStatus httpStatus;

    public PriceException(String message, Throwable cause, HttpStatus httpStatus) {
        super(message, cause);
        this.httpStatus = httpStatus;
    }

    public PriceException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }
}
