package by.epam.final_project.soap.gen;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.Objects;

/**
 * <p>Java class for price complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="price">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="product_id" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="currency" type="{http://www.epam.by/final_project/soap/gen}currencyEnum"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "price", propOrder = {
        "productId",
        "value",
        "currency"
})
public class Price implements Serializable {
    @XmlElement(name = "product_id")
    private Long productId;
    private int value;
    @XmlElement(required = true)
    private CurrencyEnum currency;

    public Price() {
        this.productId = null;
        this.value = 0;
        this.currency = null;
    }

    /**
     *  Modify construct 2 param for json response
     * @param value value price
     * @param currency type Currency
     */
    public Price(int value, String currency) {
        this.productId = null;
        this.value = value;
        this.currency = CurrencyEnum.fromValue(currency);
    }

    /**
     *  Modify construct 2 param for json response
     * @param value value price
     * @param currency type Currency
     */
    public Price(String value, String currency) {
        this.productId = null;
        this.value = Integer.parseInt(value);
        this.currency = CurrencyEnum.fromValue(currency);
    }

    /**
     *  Modify construct 3 param for json response
     * @param product_id id Product
     * @param value value price
     * @param currency type Currency
     */
    public Price(String product_id, String value, String currency) {
        this.productId = Long.valueOf(product_id);
        this.value = Integer.parseInt(value);
        this.currency = CurrencyEnum.fromValue(currency);
    }

    public Price(Long product_id, int value, CurrencyEnum currency) {
        this.productId = product_id;
        this.value = value;
        this.currency = currency;
    }

    /**
     * Gets the value of the productId property.
     *
     */
    public long getProductId() {
        return productId;
    }

    /**
     * Sets the value of the productId property.
     *
     */
    public void setProductId(long value) {
        this.productId = value;
    }

    /**
     * Gets the value of the value property.
     *
     */
    public int getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     *
     */
    public void setValue(int value) {
        this.value = value;
    }

    /**
     * Gets the value of the currency property.
     *
     * @return
     *     possible object is
     *     {@link CurrencyEnum }
     *
     */
    public CurrencyEnum getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     *
     * @param value
     *     allowed object is
     *     {@link CurrencyEnum }
     *
     */
    public void setCurrency(CurrencyEnum value) {
        this.currency = value;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Price)) return false;
        Price price = (Price) o;
        return currency == price.currency &&
                Objects.equals(productId, price.productId);
    }

    @Override
    public int hashCode() {
        return currency.ordinal();
    }

}
