package by.epam.final_project.soap;

import by.epam.final_project.Const;
import by.epam.final_project.soap.gen.*;
import org.apache.log4j.Logger;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

public class PriceClient extends WebServiceGatewaySupport {
    private static final Logger LOGGER = Logger.getLogger(PriceClient.class);
    private static final String SOAP_ACTION_CALLBACK = "http://www.epam.by/final_project/soap/gen";

    public GetPriceResponse getPrice(Long productID) {
        LOGGER.info("IN PriceClient method getPrice");
        GetPriceRequest request = new GetPriceRequest();
        request.setProductId(productID);
        LOGGER.info("Requesting prices for " + productID);
        return (GetPriceResponse) getWebServiceTemplate()
                .marshalSendAndReceive(
                        Const.URL_SOAP_SERVICE_PRICE,
                        request,
                        new SoapActionCallback(SOAP_ACTION_CALLBACK + "/GetPriceRequest"));
    }


    public GetPricesRangeResponse getPricesRange(Long minValue, Long maxValue) {
        LOGGER.info("IN PriceClient method getPricesRange");
        GetPricesRangeRequest request = new GetPricesRangeRequest();
        request.setMinValue(minValue);
        request.setMaxValue(maxValue);
        LOGGER.info("Requesting prices range for " + minValue +";" + maxValue);
        return (GetPricesRangeResponse) getWebServiceTemplate()
                .marshalSendAndReceive(
                        Const.URL_SOAP_SERVICE_PRICE,
                        request,
                        new SoapActionCallback(SOAP_ACTION_CALLBACK + "/GetPricesRangeRequest"));
    }

    public CreatePriceResponse createPrice(Long productID, int value, CurrencyEnum currency) {
        LOGGER.info("IN PriceClient method createPrice");
        CreatePriceRequest request = new CreatePriceRequest();
        request.setProductId(productID);
        request.setValue(value);
        request.setCurrency(currency);
        LOGGER.info("Requesting create prices for " + productID + ";" + value+ ";" + currency);
        return (CreatePriceResponse) getWebServiceTemplate()
                .marshalSendAndReceive(
                        Const.URL_SOAP_SERVICE_PRICE,
                        request,
                        new SoapActionCallback(SOAP_ACTION_CALLBACK + "/CreatePriceRequest"));
    }

    public UpdatePriceResponse updatePrice(Long productID, int value, CurrencyEnum currency) {
        LOGGER.info("IN PriceClient method updatePrice");
        UpdatePriceRequest request = new UpdatePriceRequest();
        request.setProductId(productID);
        request.setValue(value);
        request.setCurrency(currency);
        LOGGER.info("Requesting update prices by " + productID + ";" + value+ ";" + currency);
        return (UpdatePriceResponse) getWebServiceTemplate()
                .marshalSendAndReceive(
                        Const.URL_SOAP_SERVICE_PRICE,
                        request,
                        new SoapActionCallback(SOAP_ACTION_CALLBACK + "/UpdatePriceRequest"));
    }

    public DeletePriceResponse deletePrice(Long productID) {
        LOGGER.info("IN PriceClient method deletePrice");
        DeletePriceRequest request = new DeletePriceRequest();
        request.setProductId(productID);
        LOGGER.info("Requesting delete prices for " + productID);
        return (DeletePriceResponse) getWebServiceTemplate()
                .marshalSendAndReceive(
                        Const.URL_SOAP_SERVICE_PRICE,
                        request,
                        new SoapActionCallback(SOAP_ACTION_CALLBACK + "/DeletePriceRequest"));
    }
}
