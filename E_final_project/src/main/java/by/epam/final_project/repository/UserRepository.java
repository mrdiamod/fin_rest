package by.epam.final_project.repository;


import by.epam.final_project.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    /**
     * find by login
     * @param login User
     * @return object User {@link User}
     */
    User findByLogin(String login);

}
