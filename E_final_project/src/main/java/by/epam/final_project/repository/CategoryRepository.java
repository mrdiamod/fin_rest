package by.epam.final_project.repository;

import by.epam.final_project.entity.Category;
import by.epam.final_project.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *  Repository interface for {@link Category} class
 */
public interface CategoryRepository extends JpaRepository<Category, Long> {
    /**
     * find by name
     * @param name Category
     * @return object Category {@link Category}
     */
    Category findByName(String name);

    /**
     * get last added row Category in H2
     * @return object Category {@link Category}
     */
    Category findFirstIdByOrderByIdDesc();
}