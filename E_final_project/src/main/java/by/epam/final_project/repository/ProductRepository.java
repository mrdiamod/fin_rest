
package by.epam.final_project.repository;

import by.epam.final_project.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 *  Repository interface for {@link Product} class
 */
public interface ProductRepository extends JpaRepository<Product, Long> {
    /**
     * get last added row Product in H2
     * @return object Product {@link Product}
     */
    Product findFirstIdByOrderByIdDesc();

}

