package by.epam.final_project.soap;

import by.epam.final_project.soap.gen.*;
import org.apache.log4j.Logger;


import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;


class PriceClientTest {
    private static final Logger LOGGER = Logger.getLogger(PriceClientTest.class);

    PriceConfiguration priceConfiguration = new PriceConfiguration();
    Jaxb2Marshaller marshaller = priceConfiguration.marshaller();
    PriceClient priceClient = priceConfiguration.priceClient(marshaller);
    Long idProduct = 666L;

    @Before
    public void init(){
        //delete
        DeletePriceResponse deleteResp = priceClient.deletePrice(idProduct);
        LOGGER.info("delete " + deleteResp.getStatus());
        //create
        CreatePriceResponse createResp = priceClient.createPrice(idProduct,50000, CurrencyEnum.PLN);
        LOGGER.info("create " + createResp.getStatus());
    }

    @Test
    void getPrice() {
        GetPriceResponse response2 = priceClient.getPrice(553243243L);
        assertNull(response2.getPrice());

        response2 = priceClient.getPrice(idProduct);
        assertNotNull(response2.getPrice());
    }

    @Test
    void setPrice() {
    }

    @Test
    void createPrice() {
        Long idProductForCreate = 667L;
        CreatePriceResponse response2 = priceClient.createPrice(idProductForCreate,50000, CurrencyEnum.PLN);
        assertEquals("OK",response2.getStatus());

        response2 = priceClient.createPrice(idProductForCreate,435345, CurrencyEnum.BYN);
        assertEquals("ERROR CREATE",response2.getStatus());

        DeletePriceResponse deletePriceResponse = priceClient.deletePrice(idProductForCreate);
        assertEquals("OK",deletePriceResponse.getStatus());
    }

    @Test
    void updatePrice() {
        init();
        UpdatePriceResponse updateResp = priceClient.updatePrice(idProduct,50001, CurrencyEnum.PLN);
        assertEquals("OK",updateResp.getStatus());
        //update not created id
        updateResp = priceClient.updatePrice(325332423L,50001, CurrencyEnum.PLN);
        assertEquals("ERROR UPDATE",updateResp.getStatus());
    }

    @Test
    void deletePrice() {
        Long idProductForDelete = 668L;
        CreatePriceResponse response2 = priceClient.createPrice(idProductForDelete,50000, CurrencyEnum.PLN);
        assertEquals("OK",response2.getStatus());

        DeletePriceResponse deletePriceResponse = priceClient.deletePrice(idProductForDelete);
        assertEquals("OK",deletePriceResponse.getStatus());

        //delete not created id
        deletePriceResponse = priceClient.deletePrice(343243242L);
        assertEquals("ERROR DELETE",deletePriceResponse.getStatus());
    }
}